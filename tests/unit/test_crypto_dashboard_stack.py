import aws_cdk as core
import aws_cdk.assertions as assertions

from crypto_dashboard.crypto_dashboard_stack import CryptoDashboardStack

# example tests. To run these tests, uncomment this file along with the example
# resource in crypto_dashboard/crypto_dashboard_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = CryptoDashboardStack(app, "crypto-dashboard")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })

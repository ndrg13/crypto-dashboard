from constructs import Construct
import json
import datetime
from aws_cdk import (
    BundlingOptions,
    Stack,
    aws_lambda,
    aws_apigateway,
    aws_s3,
    aws_s3_deployment,
    RemovalPolicy,
    aws_cognito,
    aws_dynamodb,
    custom_resources,
    aws_events
)


class CryptoDashboardStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        """dynamo = aws_dynamodb.Table(
            self, 'TableDCA',
            partition_key=

        )"""

        lambda_ = aws_lambda.Function(
            self,
            "TestTrading",
            runtime=aws_lambda.Runtime.PYTHON_3_9,
            handler="trading.newOrder",
            code=aws_lambda.Code.from_asset(
                "lambda",
            ),
        )

        eventRule = aws_events.Rule(
            self, 'LambdaRule',
            schedule=aws_events.Schedule.cron(
                minute='0',
                hour='1'
            )
        )

        """lambda_ = aws_lambda.Function(
            self,
            "HelloHandler",
            runtime=aws_lambda.Runtime.PYTHON_3_9,
            handler="trading",
            code=aws_lambda.Code.from_asset(
                "lambda",
                bundling=BundlingOptions(
                    image=aws_lambda.Runtime.PYTHON_3_9.bundling_image,
                    command=[
                        "bash",
                        "-c",
                        "pip install --no-cache -r requirements.txt -t /asset-output && cp -au . /asset-output",
                    ],
                ),
            ),
        )"""

        api_gw = aws_apigateway.LambdaRestApi(self, "HTTP API", handler=lambda_)

        test_resource = api_gw.root.add_resource("test")
        test_resource.add_method("GET", aws_apigateway.LambdaIntegration(lambda_))

        """default_cors_preflight_options=aws_apigateway.CorsOptions(
                allow_origins=aws_apigateway.Cors.ALL_ORIGINS
            )"""

        s3 = aws_s3.Bucket(
            self,
            "Frontend",
            public_read_access=True,
            removal_policy=RemovalPolicy.DESTROY,
            auto_delete_objects=True,
            website_index_document="index.html",
        )

        s3.add_cors_rule(
            allowed_origins=["*"],
            allowed_headers=["*"],
            allowed_methods=[
                aws_s3.HttpMethods.GET,
                aws_s3.HttpMethods.POST,
                aws_s3.HttpMethods.PUT,
                aws_s3.HttpMethods.HEAD,
                aws_s3.HttpMethods.DELETE,
            ],
        )

        s3_deploy = aws_s3_deployment.BucketDeployment(
            self,
            "DeployWebsite",
            sources=[aws_s3_deployment.Source.asset("./static")],
            destination_bucket=s3,
            destination_key_prefix="web/static",
        )

        frontend_config = {
            "url": api_gw.url,
            "lastChanged": datetime.datetime.utcnow().isoformat(),
        }

        data_string = "export const apigwUrl = " + json.dumps(frontend_config)

        custom_resource = custom_resources.AwsCustomResource(
            self,
            "AwsCustomResource",
            on_create=custom_resources.AwsSdkCall(
                service="S3",
                action="putObject",
                parameters={
                    "Body": data_string,
                    "Bucket": s3.bucket_name,
                    "Key": "web/static/js/apigwUrl.js",
                    "ContentType": "application/javascript",
                },
                physical_resource_id=custom_resources.PhysicalResourceId.of(
                    s3.bucket_name
                ),
            ),
            on_update=custom_resources.AwsSdkCall(
                service="S3",
                action="putObject",
                parameters={
                    "Body": data_string,
                    "Bucket": s3.bucket_name,
                    "Key": "web/static/js/apigwUrl.js",
                    "ContentType": "application/javascript",
                },
                physical_resource_id=custom_resources.PhysicalResourceId.of(
                    s3.bucket_name
                ),
            ),
            policy=custom_resources.AwsCustomResourcePolicy.from_sdk_calls(
                resources=custom_resources.AwsCustomResourcePolicy.ANY_RESOURCE
            ),
        )

        custom_resource.node.add_dependency(s3_deploy)
        custom_resource.node.add_dependency(api_gw)

        user_pool = aws_cognito.UserPool(
            self,
            "UserPool1",
            self_sign_up_enabled=True,
            sign_in_aliases=aws_cognito.SignInAliases(username=True, email=True),
            auto_verify=aws_cognito.AutoVerifiedAttrs(email=True, phone=True),
            removal_policy=RemovalPolicy.DESTROY,
        )

        user_pool_client = user_pool.add_client(
            "UserPool1Client",
            auth_flows=aws_cognito.AuthFlow(
                admin_user_password=True,
                user_password=True,
            ),
            o_auth=aws_cognito.OAuthSettings(
                callback_urls=[
                    "https://cryptodashboardstack-frontend23d93c55-7vhn97fek1w3.s3.eu-north-1.amazonaws.com/web/static/html/index.html"
                ],
                logout_urls=[
                    "https://cryptodashboardstack-frontend23d93c55-7vhn97fek1w3.s3.eu-north-1.amazonaws.com/web/static/html/index.html"
                ],
            ),
        )

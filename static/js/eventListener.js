import { modal1 } from './modal.js'
import { scheduleDCA } from './exchangeHandler.js'

document.getElementById('calendarButton').addEventListener('click', () => {
    document.getElementById('dashboardDiv').style.display = 'none'
    document.getElementById('settingDiv').style.display = 'none'
    document.getElementById('calendarDiv').style.display = 'flex'
})

document.getElementById('dashboardButton').addEventListener('click', () => {
    document.getElementById('calendarDiv').style.display = 'none'
    document.getElementById('settingDiv').style.display = 'none'
    document.getElementById('dashboardDiv').style.display = 'flex'
})

document.getElementById('settingButton').addEventListener('click', () => {
    document.getElementById('dashboardDiv').style.display = 'none'
    document.getElementById('calendarDiv').style.display = 'none'
    document.getElementById('settingDiv').style.display = 'flex'
})

document.getElementById('btn-close-1').addEventListener('click', () => {
    modal1.hide()
})

document.getElementById('scheduleDCA').addEventListener('click', () => {
    modal1.hide()
    scheduleDCA()
})
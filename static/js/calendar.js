import { modal1 } from "./modal.js" 

export const calElem = document.getElementById('calendar')

const cal = new FullCalendar.Calendar(calElem, {
    initialView: 'timeGridWeek',
    height: '100%',
    expandRows: true,
    views: {
        timeGridWeek: {
            slotDuration: '01:00:00'
        }
    },
    dateClick: (info) => {
        modal1.toggle()
        document.getElementById('selectedDate').value = info.dateStr
    }
})

export const getCalendar = () => {
    return cal
}
import { buildPairList } from "./DOMHandler.js"
import { getFormData } from "./DOMHandler.js";
import { addEvent } from "./calendarEvent.js";

export const getAllPair = async () => {
    try {
        const resp = await fetch('https://testnet.binance.vision/api/v3/exchangeInfo', {
            method: 'GET'
        })

        if (resp.ok) {
            await resp.json().then(data => buildPairList(data['symbols']))
        } else {
            await resp.json().then(data => console.log(data))
        }
        
    } catch (e) {
        console.log(e)
    }
}

const calculateEndDate = (formData) => {
    if (formData['endScheduleType'] == 'week') {
        let endDate = new Date()
        endDate.setDate(endDate.getDate() + parseInt(formData['endScheduleNumber']) * 7 + 1)
        return endDate.toISOString()
    } else if (formData['endScheduleType'] == 'month') {
        let endDate = new Date()
        endDate.setMonth(endDate.getMonth() + parseInt(formData['endScheduleNumber']))
        return endDate.toISOString()
    } else if (formData['endScheduleType'] == 'year') {
        let endDate = new Date()
        endDate.setFullYear(endDate.getFullYear() + parseInt(formData['endScheduleNumber']))
        return endDate.toISOString()
    }
}

const sendOrder = (event, formData, endDate) => {
    try {
        /*const resp = await fetch('https://testnet.binance.vision/api/v3/order?' , {
            method: 'POST',
            headers: new Headers({
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Headers': '*',
                'X-MBX-APIKEY': apiKey
            })
        })

        await resp.json().then(data => console.log(data))*/

        //return updateScheduleEvent(event)

        event.remove()
        return addEvent(formData, endDate)


    } catch (e) {
        console.log(e)
    }
}

export const scheduleDCA = () => {
    const formData = getFormData()
    const endDate = calculateEndDate(formData)
    let event = addEvent(formData, endDate, true)
    let newEvent = sendOrder(event, formData, endDate)
}

/*const getJson = async (url = null, requestOptions = null) => {
    return await fetch(url, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            } else {
                const jsoned = response.json();
                return jsoned;
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}

const serverTimestamp = async () => {
    const url = 'https://testnet.binance.vision/api/v3/time';
    const timeServer = await getJson(url);
    return timeServer.serverTime;
}

const newOrderString = async (symbol, quantity, secretKey) => {
    const timestamp = await serverTimestamp()
        .then(timestamp => {
            return timestamp;
        });

    let query = 'symbol=' + symbol + '&side=BUY&type=MARKET&quantity=' + quantity + '&timestamp=' + timestamp

    return query + '&signature=' + CryptoJS.HmacSHA256(query, secretKey).toString(CryptoJS.enc.Hex)
}

const signKey = (queryString, secretKey) => {
    const key = CryptoJS.enc.Utf8.parse(secretKey)
    const txt = CryptoJS.enc.Utf8.parse(queryString)
    const hmac = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(txt, key))

    //return queryString += '&signature=' + crypto.createHmac('sha256', secretKey).update(queryString).digest('hex')
}

const sendOrder = async (apiKey, secretKey) => {
    console.log('https://testnet.binance.vision/api/v3/order?' + signKey(newOrderString(getFormData()['pair'], getFormData()['amount']), secretKey))

    try {
        const resp = await fetch('https://testnet.binance.vision/api/v3/order?' + newOrderString(getFormData()['pair'], getFormData()['amount'], secretKey)//signKey(newOrderString(getFormData()['pair'], getFormData()['amount']), secretKey), {
            method: 'POST',
            headers: new Headers({
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Headers': '*',
                'X-MBX-APIKEY': apiKey
            })
        })

        await resp.json().then(data => console.log(data))

    } catch (e) {
        console.log(e)
    }
}*/



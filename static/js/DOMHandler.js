import { scheduleDCA } from "./exchangeHandler.js"

export const uncheckGroupCheckbox = () => {
    const pairMenu = document.getElementById('dropdownMenu')

    for (let node of pairMenu.children) {
        node.childNodes[1].checked = false
    }
}

export const buildPairList = (pairList) => {
    /*const pairMenu = document.getElementById('dropdownMenu')
    
    while (pairMenu.lastElementChild) {
        pairMenu.removeChild(pairMenu.lastElementChild)
    }

    for (let i = 0; i < pairList.length; i++) {
        let pairDiv = document.createElement('li')
        pairDiv.style.display = 'flex'
        pairDiv.style.paddingRight = '1rem'

        let pairName = document.createElement('a')
        pairName.textContent = pairList[i]['symbol']
        pairName.href = '#'

        let pairCheckbox = document.createElement('input')
        pairCheckbox.id = 'pairCheckbox' + i
        pairCheckbox.type = 'checkbox'
        pairCheckbox.className = 'form-check-input'
        pairCheckbox.name = 'pairCheckbox'
        pairCheckbox.value = pairList[i]['symbol']

        pairMenu.appendChild(pairDiv)
        pairDiv.appendChild(pairName)
        pairDiv.appendChild(pairCheckbox)
    }*/

    const sel = document.getElementById('selectPair')

    while (sel.lastElementChild) {
        sel.removeChild(sel.lastElementChild)
    }

    for (let i = 0; i < pairList.length; i++) {
        sel.add(new Option(pairList[i]['symbol'], pairList[i]['symbol']))
    }
}

export const searchField = () => {
    let input, filter, ul, li, a, i, txtValue
    input = document.getElementById('searchField')
    filter = input.value.toUpperCase()
    ul = document.getElementById('dropdownMenu')
    li = ul.getElementsByTagName('li')

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName('a')[0]
        txtValue = a.textContent || a.innerText
        
        if (txtValue.toUpperCase().indexOf(filter) > - 1) {
            li[i].style.display = 'flex'
        } else {
            li[i].style.display = 'none'
        }
    }
}

export const getFormData = () => {
    return ({
        'pair': document.getElementById('selectPair').value,
        'amount': document.getElementById('amount').value,
        'recurrence': document.querySelector('input[name="radioRecurrence"]:checked').value,
        'endScheduleNumber': document.getElementById('fieldEndSchedule').value,
        'endScheduleType': document.getElementById('selectEndSchedule').value,
        'selectedDate': document.getElementById('selectedDate').value
    })
}
import { getCalendar } from "./calendar.js";

export const addEvent = (formData, endDate, temp) => {
    return getCalendar().addEvent({
        'id': formData['selectedDate'],
        'display': 'auto',
        'title': temp? 'Adding in progress...' : formData['pair'],
        'backgroundColor': temp ? '#ffc107' : '#198754',
        'pair': formData['pair'],
        //'rrule': "DTSTART:20220610T160000Z\nRRULE:FREQ=WEEKLY;UNTIL=20230730T180000Z;INTERVAL=1;WKST=MO"
        'rrule': {
            freq: formData['recurrence'],
            interval: 1,
            dtstart: formData['selectedDate'],
            until: endDate
        },
    })
}

/*export const updateScheduleEvent = (event) => {
    console.log(event)
    event.setExtendedProp('title', event.extendedProps.pair)
    event.setProp('color', '#198754')

    return event
}*/
import { getCalendar } from './calendar.js'
import { apigwUrl } from './apigwUrl.js'
import { getAllPair } from './exchangeHandler.js'

document.addEventListener('DOMContentLoaded', () => {
    getCalendar().render()
    getAllPair()
})

const testLambda = async () => {
    try {
        const resp = await fetch(apigwUrl['url'] + 'test', {
            method: 'GET'
            /*headers: new Headers({
                'Access-Control-Allow-Origin': '*'
            })*/
        })

        if (resp.ok) {
            await resp.json().then(data => console.log(data))
        } else {
            await resp.json().then(data => console.log(data))
        }
    } catch (e) {
        console.log(e)
    }
}

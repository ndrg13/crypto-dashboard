
Bienvenue sur le projet Crypto Dashboard !

Crypto Dashboard est une application qui peut être utilisée pour monitorer un ou plusieurs comptes de trading sur exchange, ainsi que pour effectuer de l'investissement programmé et récurrent (DCA pour Dollar Cost Average).

L'infrastructure de ce projet a été intégralement créée sur AWS à l'aide de l'outil d'infrastructure as code Cloud Development Kit (CDK v2).

Crypto Dashboard est une application basée sur une architecure serverless.

Voici un schéma représentant globalement l'architecture de l'application Crypto Dashboard :

![](image/aws-schema.png)

Les avantages des architectures serverless :
    -   productivité améliorée et time-to-market réduit
    -   réduction des coûts d'exploitation
    -   scalabilité horizontale et verticale dynamique
    -   évolutivité et infrastructure as code
    -   zone de disponibilités étendues (latence réduite)

Liste des services utilisés dans ce projet : 
    -   AWS S3 storage (stockage static : frontend)
    -   AWS Lambda (Function as a service : backend)
    -   AWS DynamoDB (BDD NoSQL clé/valeur)
    -   AWS ApiGateway (routage frontend -> backend)
    -   AWS Cognito (authentification)

URL : https://cryptodashboardstack-frontend23d93c55-7vhn97fek1w3.s3.eu-north-1.amazonaws.com/web/static/html/index.html

Nicolas Droissart 2022
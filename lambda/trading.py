import json
from binance.spot import Spot

def newOrder(event, context):
    print('request: {}'.format(json.dumps(event)))

    client = Spot()
    client_time = client.time()

    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': '*'
        },
        'body': json.dumps(str(client_time))
    }